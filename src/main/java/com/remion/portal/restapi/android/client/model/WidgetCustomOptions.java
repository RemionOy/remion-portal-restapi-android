/**
 * DeviceApiPortal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.remion.portal.restapi.android.client.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class WidgetCustomOptions  {
  
  @SerializedName("customType")
  private String customType = null;
  @SerializedName("parameters")
  private String parameters = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCustomType() {
    return customType;
  }
  public void setCustomType(String customType) {
    this.customType = customType;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getParameters() {
    return parameters;
  }
  public void setParameters(String parameters) {
    this.parameters = parameters;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WidgetCustomOptions widgetCustomOptions = (WidgetCustomOptions) o;
    return (customType == null ? widgetCustomOptions.customType == null : customType.equals(widgetCustomOptions.customType)) &&
        (parameters == null ? widgetCustomOptions.parameters == null : parameters.equals(widgetCustomOptions.parameters));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (customType == null ? 0: customType.hashCode());
    result = 31 * result + (parameters == null ? 0: parameters.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class WidgetCustomOptions {\n");
    
    sb.append("  customType: ").append(customType).append("\n");
    sb.append("  parameters: ").append(parameters).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
