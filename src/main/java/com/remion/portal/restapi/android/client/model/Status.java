/**
 * DeviceApiPortal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.remion.portal.restapi.android.client.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class Status  {
  
  @SerializedName("text")
  private String text = null;
  @SerializedName("code")
  private Integer code = null;
  @SerializedName("level")
  private String level = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Integer getCode() {
    return code;
  }
  public void setCode(Integer code) {
    this.code = code;
  }

  /**
   * One of {OK, WARN, ERROR, FATAL}
   **/
  @ApiModelProperty(value = "One of {OK, WARN, ERROR, FATAL}")
  public String getLevel() {
    return level;
  }
  public void setLevel(String level) {
    this.level = level;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Status status = (Status) o;
    return (text == null ? status.text == null : text.equals(status.text)) &&
        (code == null ? status.code == null : code.equals(status.code)) &&
        (level == null ? status.level == null : level.equals(status.level));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (text == null ? 0: text.hashCode());
    result = 31 * result + (code == null ? 0: code.hashCode());
    result = 31 * result + (level == null ? 0: level.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Status {\n");
    
    sb.append("  text: ").append(text).append("\n");
    sb.append("  code: ").append(code).append("\n");
    sb.append("  level: ").append(level).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
