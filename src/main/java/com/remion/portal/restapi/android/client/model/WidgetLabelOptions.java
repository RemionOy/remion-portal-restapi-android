/**
 * DeviceApiPortal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.remion.portal.restapi.android.client.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class WidgetLabelOptions  {
  
  @SerializedName("text")
  private String text = null;
  @SerializedName("title")
  private String title = null;
  @SerializedName("icon")
  private String icon = null;
  @SerializedName("color")
  private String color = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getIcon() {
    return icon;
  }
  public void setIcon(String icon) {
    this.icon = icon;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getColor() {
    return color;
  }
  public void setColor(String color) {
    this.color = color;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WidgetLabelOptions widgetLabelOptions = (WidgetLabelOptions) o;
    return (text == null ? widgetLabelOptions.text == null : text.equals(widgetLabelOptions.text)) &&
        (title == null ? widgetLabelOptions.title == null : title.equals(widgetLabelOptions.title)) &&
        (icon == null ? widgetLabelOptions.icon == null : icon.equals(widgetLabelOptions.icon)) &&
        (color == null ? widgetLabelOptions.color == null : color.equals(widgetLabelOptions.color));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (text == null ? 0: text.hashCode());
    result = 31 * result + (title == null ? 0: title.hashCode());
    result = 31 * result + (icon == null ? 0: icon.hashCode());
    result = 31 * result + (color == null ? 0: color.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class WidgetLabelOptions {\n");
    
    sb.append("  text: ").append(text).append("\n");
    sb.append("  title: ").append(title).append("\n");
    sb.append("  icon: ").append(icon).append("\n");
    sb.append("  color: ").append(color).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
