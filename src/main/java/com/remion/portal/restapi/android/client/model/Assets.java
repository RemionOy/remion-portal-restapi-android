/**
 * DeviceApiPortal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.remion.portal.restapi.android.client.model;

import com.remion.portal.restapi.android.client.model.Asset;
import java.util.*;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class Assets  {
  
  @SerializedName("assets")
  private List<Asset> assets = null;
  @SerializedName("totalCount")
  private Long totalCount = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public List<Asset> getAssets() {
    return assets;
  }
  public void setAssets(List<Asset> assets) {
    this.assets = assets;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Long getTotalCount() {
    return totalCount;
  }
  public void setTotalCount(Long totalCount) {
    this.totalCount = totalCount;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Assets assets = (Assets) o;
    return (assets == null ? assets.assets == null : assets.equals(assets.assets)) &&
        (totalCount == null ? assets.totalCount == null : totalCount.equals(assets.totalCount));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (assets == null ? 0: assets.hashCode());
    result = 31 * result + (totalCount == null ? 0: totalCount.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Assets {\n");
    
    sb.append("  assets: ").append(assets).append("\n");
    sb.append("  totalCount: ").append(totalCount).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
