/**
 * DeviceApiPortal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.remion.portal.restapi.android.client.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class MigrationResult  {
  
  @SerializedName("result")
  private String result = null;
  @SerializedName("dryRun")
  private Boolean dryRun = null;
  @SerializedName("success")
  private Boolean success = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getResult() {
    return result;
  }
  public void setResult(String result) {
    this.result = result;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Boolean getDryRun() {
    return dryRun;
  }
  public void setDryRun(Boolean dryRun) {
    this.dryRun = dryRun;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Boolean getSuccess() {
    return success;
  }
  public void setSuccess(Boolean success) {
    this.success = success;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MigrationResult migrationResult = (MigrationResult) o;
    return (result == null ? migrationResult.result == null : result.equals(migrationResult.result)) &&
        (dryRun == null ? migrationResult.dryRun == null : dryRun.equals(migrationResult.dryRun)) &&
        (success == null ? migrationResult.success == null : success.equals(migrationResult.success));
  }

  @Override
  public int hashCode() {
    Integer result = 17;
    result = 31 * result + (result == null ? 0: result.hashCode());
    result = 31 * result + (dryRun == null ? 0: dryRun.hashCode());
    result = 31 * result + (success == null ? 0: success.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class MigrationResult {\n");
    
    sb.append("  result: ").append(result).append("\n");
    sb.append("  dryRun: ").append(dryRun).append("\n");
    sb.append("  success: ").append(success).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
