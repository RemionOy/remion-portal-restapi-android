
# UISettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**footer** | **String** |  |  [optional]
**logo** | [**Image**](Image.md) |  |  [optional]



