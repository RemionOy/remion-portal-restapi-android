
# WidgetLabelOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**icon** | **String** |  |  [optional]
**color** | **String** |  |  [optional]



