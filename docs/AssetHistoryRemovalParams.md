
# AssetHistoryRemovalParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**signals** | **Boolean** |  |  [optional]
**events** | **Boolean** |  |  [optional]
**locations** | **Boolean** |  |  [optional]
**files** | **Boolean** |  |  [optional]



