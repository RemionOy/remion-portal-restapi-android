
# Metadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**datatype** | [**DatatypeEnum**](#DatatypeEnum) |  |  [optional]
**datatypeOptions** | **String** |  |  [optional]
**isReadonly** | **Boolean** |  |  [optional]


<a name="DatatypeEnum"></a>
## Enum: DatatypeEnum
Name | Value
---- | -----



