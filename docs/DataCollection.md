
# DataCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**names** | **List&lt;String&gt;** | Names of signals |  [optional]
**valueRows** | [**List&lt;DataValueRow&gt;**](DataValueRow.md) | Values of signals in the same order as &#39;names&#39;. One SampleValueRow per timestamp. |  [optional]



