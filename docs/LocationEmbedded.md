
# LocationEmbedded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Long** | Unix time in ms |  [optional]
**latitude** | **Double** | North &#x3D; positive |  [optional]
**longitude** | **Double** | East &#x3D; positive |  [optional]



