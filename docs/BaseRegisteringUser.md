
# BaseRegisteringUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**username** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**language** | **String** |  |  [optional]
**recaptcha** | **String** |  |  [optional]



