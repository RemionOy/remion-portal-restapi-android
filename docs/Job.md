
# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**type** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**targetAssetIds** | [**List&lt;ObjectId&gt;**](ObjectId.md) |  |  [optional]



