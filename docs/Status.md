
# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** |  |  [optional]
**code** | **Integer** |  |  [optional]
**level** | **String** | One of {OK, WARN, ERROR, FATAL} |  [optional]



