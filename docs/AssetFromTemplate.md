
# AssetFromTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**template** | **String** |  |  [optional]
**parent** | **String** |  |  [optional]



