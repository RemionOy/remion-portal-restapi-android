
# AssetFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**timestamp** | **Long** | Creation time. Unix time in ms |  [optional]
**filename** | **String** |  |  [optional]
**content** | **List&lt;byte[]&gt;** |  |  [optional]
**contentType** | **String** |  |  [optional]
**contentLength** | **Long** |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**md5Hash** | **String** |  |  [optional]
**parentAsset** | [**ReferencedId**](ReferencedId.md) |  |  [optional]



