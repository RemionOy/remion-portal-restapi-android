
# Action

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**type** | **String** |  |  [optional]
**targetAsset** | [**ReferencedId**](ReferencedId.md) |  |  [optional]



