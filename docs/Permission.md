
# Permission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**permissionType** | [**PermissionTypeEnum**](#PermissionTypeEnum) |  |  [optional]


<a name="PermissionTypeEnum"></a>
## Enum: PermissionTypeEnum
Name | Value
---- | -----



