
# WidgetCanvasOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bgColor** | **String** |  |  [optional]
**bgImage** | [**Image**](Image.md) |  |  [optional]
**items** | [**List&lt;WidgetCanvasItem&gt;**](WidgetCanvasItem.md) |  |  [optional]



