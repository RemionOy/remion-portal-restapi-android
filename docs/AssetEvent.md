
# AssetEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**startTime** | **Long** | Unix time in ms |  [optional]
**endTime** | **Long** | Unix time in ms |  [optional]
**type** | **String** |  |  [optional]
**eventId** | **Integer** |  |  [optional]
**textId** | **String** |  |  [optional]
**sourceId** | **Integer** |  |  [optional]
**sourceTextId** | **String** |  |  [optional]
**parent** | [**ReferencedId**](ReferencedId.md) |  |  [optional]



