
# ObjectId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Integer** |  |  [optional]
**machineIdentifier** | **Integer** |  |  [optional]
**processIdentifier** | **Integer** |  |  [optional]
**counter** | **Integer** |  |  [optional]
**time** | **Long** |  |  [optional]
**date** | [**Date**](Date.md) |  |  [optional]
**timeSecond** | **Integer** |  |  [optional]



