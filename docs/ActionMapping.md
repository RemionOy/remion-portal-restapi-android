
# ActionMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | [**Event**](Event.md) |  |  [optional]
**action** | [**Action**](Action.md) |  |  [optional]



